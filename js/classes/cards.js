function createCards(playerNumber) {
    let playerId = $('.player')[playerNumber].getAttribute('id')
    let cardElt = jQuery('<div></div>')
    cardElt.attr({
        id: `${playerId}Card`,
    })
    cardElt.css({
        width: '300px',
        height: '500px',
        position: 'absolute',
    })

    if (playerNumber === 0) {
        $('#content').prepend(cardElt)
    } else {
        $('#content').append(cardElt)
    }

    let playerDescription = jQuery('<span></span>')
    playerDescription.attr({
        id:`playerCard${playerNumber}`
    })
    playerDescription.css({
        color: 'white',
        fontSize: '2em',
        fontFamily: 'calibri'
    })

    let armeDescription = jQuery('<span></span>')
    armeDescription.css({
        color: 'white',
        fontSize: '2em',
        fontFamily: 'calibri'
    })
    armeDescription.attr({
        id: `armeDescription${playerNumber}`
    })
    if (playerId === 'player1') {
        playerDescription.text(player1.decrire());
        armeDescription.text(player1.armePortee());
    } else {
        playerDescription.text(player2.decrire());
        armeDescription.text(player2.armePortee());
    }

    cardElt.append(playerDescription)
    cardElt.append(armeDescription)
}
