function endBattle() {
    console.log('ok')
    let endMessage = jQuery('<div></div>')
    endMessage.attr({
        id: 'endMessage'
    })
    endMessage.css({
        width: '200px',
        height: '100px',
        fontSize: '1em',
        fontFamily: 'calibri',
        color: 'black',
        backgroundColor: 'white',
        position: 'absolute',
        left: '17.5em',
        top: '15em',
        zIndex: '12',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    })
    endMessage.text('Le combat est terminé !')
    $('#battlefield').append(endMessage);
}
