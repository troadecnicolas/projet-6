let fight = (function () {
    let executed = false;
    return function () {
        if (!executed) {
            executed = true;
            let battlefieldElt = jQuery('<div></div>')
            battlefieldElt.attr({
                id: 'battlefield'
            })
            battlefieldElt.css({
                width: '720px',
                height: '720px',
                position: 'absolute',
                backgroundImage: 'url(images/paysage.gif)',
                backgroundPosition: 'center',
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'contain',
                top: '5em',
                zIndex: '30'
            })
            
            $('#plateau').append(battlefieldElt);

            let player1attack = jQuery('<button>Zelda attaque</button>')
            player1attack.attr({
                id: 'player1attack',
                class: 'buttonGame'
            })
            player1attack.css({
                width: '100px',
                height: '50px',
                position: 'absolute',
                top: '30em',
                left: '10em',
                zIndex: '11'
            })
            $('#battlefield').append(player1attack);

            let player2attack = jQuery('<button>Miko attaque</button>')
            player2attack.attr({
                id: 'player2attack',
                class: 'buttonGame',
                disabled: 'true'
            })
            player2attack.css({
                width: '100px',
                height: '50px',
                position: 'absolute',
                top: '30em',
                left: '40em',
                zIndex: '11'
            })
            $('#battlefield').append(player2attack);

            $('#player1attack').click(() => {
                player1.attaquer(player2)
                $('#playerCard1').text(player2.decrire())
                $('#player2attack').removeAttr('disabled')
                $('#player1attack').attr('disabled', 'true')
                $('#player2defense').removeAttr('disabled')
                $('#player1defense').attr('disabled', 'true')
            })

            $('#player2attack').click(() => {
                player2.attaquer(player1)
                $('#playerCard0').text(player1.decrire())
                $('#player1attack').removeAttr('disabled')
                $('#player2attack').attr('disabled', 'true')
                $('#player1defense').removeAttr('disabled')
                $('#player2defense').attr('disabled', 'true')
            })


            let player1defense = jQuery('<button>Zelda se défend</button>')
            player1defense.attr({
                id: 'player1defense',
                class: 'buttonGame'

            })
            player1defense.css({
                width: '100px',
                height: '50px',
                position: 'absolute',
                top: '20em',
                left: '10em',
                zIndex: '11'
            })
            $('#battlefield').append(player1defense);

            let player2defense = jQuery('<button>Miko se défend</button>')
            player2defense.attr({
                id: 'player2defense',
                disabled: 'true',
                class: 'buttonGame'

            })
            player2defense.css({
                width: '100px',
                height: '50px',
                position: 'absolute',
                top: '20em',
                left: '40em',
                zIndex: '11'
            })
            $('#battlefield').append(player2defense);

            $('#player1defense').click(() => {
                player1.defense = true
                $('#player2attack').removeAttr('disabled')
                $('#player2defense').removeAttr('disabled')
                $('#player1attack').attr('disabled', 'true')
                $('#player1defense').attr('disabled', 'true')
            })

            $('#player2defense').click(() => {
                player2.defense = true
                $('#player1attack').removeAttr('disabled')
                $('#player1defense').removeAttr('disabled')
                $('#player2attack').attr('disabled', 'true')
                $('#player2defense').attr('disabled', 'true')
            })
        }
    };
})();