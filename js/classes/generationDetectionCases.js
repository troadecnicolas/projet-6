function genererPlateau() {

    let plateau = $('#plateau');
    let body = $('#bodyElt');
    let surcouche = $('#surcouche')
    for (i = 0; i <= 9; i++) {
        let tr = jQuery('<tr></tr>');
        tr.attr('id', i).attr('class', 'trClass');
        plateau.append(tr);
        for (j = 0; j <= 9; j++) {
            let td = jQuery('<td></td>');
            td.attr({
                id: j,
                class: 'nonBlocked',
                'disponible': true,
                'toMove': false
                //    position: 'absolute'
            });
            tr.append(td);
        }
    };
}

function randomisePlateau() {
    $('td').each(function () {
        let a = Math.random();
        if (a > 0.9) {
            $(this).css({
                backgroundImage: 'url(images/tiles/blocked.png)',
                backgroundSize: 'contain'
            });
            $(this).attr({
                class: 'blocked',
                disponible: false
                //   position: 'absolute'
            });
        } else {
            let arrayOfTiles = ['url(images/tiles/tiles1.png)', 'url(images/tiles/tiles2.png)', 'url(images/tiles/tiles3.png)', 'url(images/tiles/tiles4.png)', 'url(images/tiles/tiles5.png)', 'url(images/tiles/tiles6.png)', 'url(images/tiles/tiles7.png)', 'url(images/tiles/tiles8.png)']
            let randomInt = Math.min(Math.round(Math.random() * 7))
            $(this).css({
                backgroundImage: arrayOfTiles[randomInt],
                backgroundSize: 'contain'
            });
        }
    })
}

function detectionCases(numeroCaseJoueur) {
    let arrTd = $('td').get(); //crée un tableau des cases non-bloquées

    let casesAVerifier = [
        [+1, +2, +3],
        [-1, -2, -3],
        [-10, -20, -30],
        [+10, +20, +30]
    ]; //permet d'indiquer le numéro des cases à vérifier autour du joueur

    let ligneCaseJoueur = Number(arrTd[numeroCaseJoueur].parentNode.id); //la ligne de la case du joueur
    let ligneAuDessous = ligneCaseJoueur + 1;
    let ligneAuDessus = ligneCaseJoueur - 1;

    let casesToMove = []; //stock les cases 'toMove' vers lesquelles le joueur peut se déplacer

    function verifierCases(casesAVerifier, direction) { //fonction qui permet de vérifier si une case existe, si elle est bloquée ou non par du décor, et si elle est sur la même ligne que le joueur

        for (let i = 0; i < casesAVerifier.length; i++) { //itère sur le tableau contenant les coordonnées des cases à verifier
            let caseVerifiee = arrTd[numeroCaseJoueur + casesAVerifier[i]]; //une caseVerifiee est une case du joueur + les coordonnées "caseAVerifier"
            switch (direction) { //premier switch qui check si on vérifie les cases à l'horizontal ou à la verticale
                case 'vertical':
                    switch (caseVerifiee) {
                        case undefined: //si la case est hors plateau
                            break
                        case caseVerifiee: //si elle existe
                            switch (caseVerifiee.getAttribute('class')) { //on vérifie son attribut "class" pour vérifier si la case est bloquée ou non
                                case 'blocked': //si bloquées, l'attribut "déplacement" passe à false
                                    caseVerifiee.setAttribute('deplacement', false)
                                    casesToMove.push(caseVerifiee); //la case part dans le tableau des cases potentiellement colorables
                                    break
                                case 'nonBlocked': //si c'est l'attribut 'nonBlocked' la case est libre
                                    caseVerifiee.setAttribute('deplacement', true)
                                    casesToMove.push(caseVerifiee); //elle y part aussi
                            }
                    }
                    case 'horizontal': //on vérifie les cases à l'horizontal
                        switch (caseVerifiee) { //si la case existe
                            case undefined:
                                break
                            case caseVerifiee:
                                switch (caseVerifiee.getAttribute('class')) {
                                    case 'blocked': //si la case est bloquée, on vérifie qu'elle est sur la même ligne
                                        switch (Number(caseVerifiee.parentNode.id)) {
                                            case (ligneAuDessous): //si elle est en dessous, pas la peine de la mettre dans le tableau des cases colorables
                                                break
                                            case (ligneAuDessus):
                                                break
                                            case ligneCaseJoueur: //si elle est bloquée mais sur la même ligne que le joueur, on la met dans le tableau avec l'attribut "false"
                                                caseVerifiee.setAttribute('deplacement', false)
                                                casesToMove.push(caseVerifiee);
                                        }
                                        break
                                    case 'nonBlocked': //si elle est libre, on vérifie la ligne puis on met "déplacement" à "true" si elle est sur la même ligne que le joueur
                                        switch (Number(caseVerifiee.parentNode.id)) {
                                            case (ligneAuDessous):
                                                break
                                            case (ligneAuDessus):
                                                break
                                            case ligneCaseJoueur:
                                                caseVerifiee.setAttribute('deplacement', true)
                                                casesToMove.push(caseVerifiee);

                                        }
                                }

                        }
            }

        }
        switch (casesToMove.length) { //ce switch permet de vérifier la taille du tableau des cases colorables et de le donner en argument à la fonction qui fera la vérification finale
            case 0: //tableau vide
                break
            case 3:
                setAttrCaseToMove(3)
                break
            case 2:
                setAttrCaseToMove(2)
                break
            case 1:
                setAttrCaseToMove(1)
        }
        casesToMove = []; //on vide le tableau à la fin de la fonction
    }

    function setAttrCaseToMove(i) { //fonction qui vérifie qu'un élément de décor ne vient pas stopper la ligne. Vérification chronologique, case par case
        switch (i) {
            case 3: //le tableau possède trois éléments
                switch (casesToMove[0].getAttribute('deplacement')) { //si le premier élément du tableau possède l'attribut "déplacement" = "false", on arrête le processus
                    case 'false':
                        break
                    case 'true': //si le 1er élément est "déplacement" = "true", on le colore en vert et on vérifie le suivant
                        casesToMove[0].setAttribute('toMove', true);
                        switch (casesToMove[1].getAttribute('deplacement')) { //vérification de la case suivante, dans un sens chronologique
                            case 'false':
                                break
                            case 'true':
                                casesToMove[1].setAttribute('toMove', true);
                                switch (casesToMove[2].getAttribute('deplacement')) { //et enfin, la dernière case
                                    case 'false':
                                        break
                                    case 'true':
                                        casesToMove[2].setAttribute('toMove', true);

                                }

                        }
                }
                case 2: //dans le cas d'un tableau contenant deux valeurs (permet d'éviter d'avoir une valeur "undefined", ce qui génère des erreurs)
                    switch (casesToMove[0].getAttribute('deplacement')) {
                        case 'false':
                            break
                        case 'true':
                            casesToMove[0].setAttribute('toMove', true);

                            switch (casesToMove[1].getAttribute('deplacement')) {
                                case 'false':
                                    break
                                case 'true':
                                    casesToMove[1].setAttribute('toMove', true);

                            }
                    }
                    case 1: //tableau avec seulement un élément
                        switch (casesToMove[0].getAttribute('deplacement')) {
                            case 'false':
                                break
                            case 'true':
                                casesToMove[0].setAttribute('toMove', true);
                        }
        }

    }

    verifierCases(casesAVerifier[0], 'horizontal')
    verifierCases(casesAVerifier[1], 'horizontal')
    verifierCases(casesAVerifier[2], 'vertical')
    verifierCases(casesAVerifier[3], 'vertical')

}



function caseInactive() {
    $("[toMove = true]").each(function (index) {
        $(this).attr('toMove', 'false');
        //   $(this).setAttribute('toMove', 'false');
    })
    $("[detection = true]").each(function (index) {
        this.setAttribute('detection', 'false')
    })
    $('.tdSurcouche').unbind();
}

function colorCaseToMove() {
    let divContainer = jQuery('<div></div>')
    divContainer.attr({
        class: 'imageToMove'
    })
    divContainer.css({
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    })
    let imageToMove = jQuery('<img>')
    imageToMove.css({
        width: '90%',
        opacity: 0.6
    });

    $(imageToMove)[0].setAttribute('src', 'images/tiles/path/tilesToMove.png')
    divContainer.append(imageToMove);
    $('[toMove = true]').append(divContainer)
    imageOverCases()
}


