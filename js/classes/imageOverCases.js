function imageOverCases() {
    let toMoveSurcouche = ($('.tdSurcouche'));
    for (i = 0; i < 100; i++) {
        if ($(toMoveSurcouche)[i].getAttribute('detection') === 'true') {
            let circleCssContainer = jQuery('<div></div>');
            circleCssContainer.attr('class', 'circleCssContainer')
            circleCssContainer.css({
                width: '70px',
                height: '70px',
                display: 'flex',
                justifyContent: 'center',
                zIndex: '30',
                opacity: '0'
            });
            let circleCss = jQuery('<div></div>');
            circleCss.css({
                marginTop: '5px',
                width: '55px',
                height: '55px',
                background: '#8EC9D7'
            });
            circleCssContainer.append(circleCss);
            $(toMoveSurcouche)[i].append(circleCssContainer[0]);
        }
    }

    $('.circleCssContainer').each(function () {
        $(this).mouseover(function () {
            $(this).css('opacity', '0.2')
        })
        $(this).mouseleave(function () {
            $(this).css({
                opacity: '0',
                transitionProperty: 'opacity',
                transitionDuration: '0.5s'
            })
        })
    })

}