function movements(numberPlayer) {
 
    let arrToMove = []; //contient les cases où le joueur peut se déplacer
    let arrSurcouche = []; //contient les 100 cases de surcouche
    let arrToMoveSurcouche = []; //contient les cases de la surcouche où le personnage peut se déplace
    let arrNumberCaseSurcouche = []; //contient les numéros des cases où le joueur peut se déplacer + 100 (numéro des cases de surcouche) 

    for (let i = 0; i < 100; i++) { //récupère les cases où le joueur peut se déplacer
        if ($('td')[i].getAttribute('toMove') === 'true') {

            arrToMove.push($('td')[i]);
        }
    }
    for (let i = 0; i < 200; i++) { //récupère les cases de surcouche
        if (($('td')[i].getAttribute('class') === 'tdSurcouche') === true) {
            arrSurcouche.push($('td')[i]);
        }
    }

    for (let i = 0; i < arrToMove.length; i++) { //récupérer les number des cases toMove
        arrNumberCaseSurcouche.push(Number(arrToMove[i].getAttribute('number')) + 100);
    }

    $(arrNumberCaseSurcouche).each(function (index) { //compare les numéros des 100 cases de surcouche avec les numéros des cases toMove + 100
        for (let i = 0; i < arrSurcouche.length; i++) {
            if (this.valueOf() === (Number(arrSurcouche[i].getAttribute('number')))) {
                arrSurcouche[i].setAttribute('detection', true)
                arrToMoveSurcouche.push(arrSurcouche[i]);
            }
        }
    })

    for (let i = 0; i < arrToMoveSurcouche.length; i++) {
        $(arrToMoveSurcouche[i]).on('click', function () {
            //recupère les coordonnées de la case sur laquelle on a cliqué
            let ancienNumeroCaseJoueur = Number($('.player')[numberPlayer].getAttribute('numberCase'));
            let nouveauNumeroCaseJoueur = Number($(arrToMove)[i].getAttribute('number'));

            $('td')[ancienNumeroCaseJoueur].setAttribute(`playeronit`, false);
            setTimeout( () => {
            $('td')[nouveauNumeroCaseJoueur].setAttribute(`playeronit`, numberPlayer + 'a');
            }, 1210)

            let playeronitCheck

            if (numberPlayer === 1) {
                playeronitCheck = '0a'
            } else {
                playeronitCheck = '1a'
            }

            $('.player')[numberPlayer].setAttribute('numbercase', nouveauNumeroCaseJoueur);
            caseInactive(); //on attribue au joueur son nouveau numéro de case

            let refDeplacement = nouveauNumeroCaseJoueur - ancienNumeroCaseJoueur; //permet de connaître la différence de numéro entre le nouveau numéro de case et l'ancien, pour déterminer dans quel sens va le joueur

            let posTop, posLeft; //initialise les variables
            let playerId = $('.player')[numberPlayer].getAttribute('id');
            switch (true) { //ce switch permet de détecter si des armes sont sur la route du joueur. Si c'est le cas elles sont envoyées au cartes sur les côtés et changent les attributs de la class joueur
                case (refDeplacement >= 10) && (refDeplacement <= 30): //si le joueur descend
                $('.imageToMove').remove()
                $('.circleCssContainer').remove()
                    weaponPlayerDetection(10, 20, 30, -1, +1, +10) //on envoie les coordonnées à vérifier à la fonction en fonction du déplacement du joueur
                    $('#img' + playerId).css({
                        transform: 'rotate(180deg)',
                        transition: 'transform 0.2s'
                    })
                    break
                case (refDeplacement <= -10) && (refDeplacement >= -30): //s'il monte
                $('.imageToMove').remove()
                $('.circleCssContainer').remove()
                    weaponPlayerDetection(-10, -20, -30, -1, +1, -10)
                    $('#img' + playerId).css({
                        transform: 'rotate(0deg)',
                        transition: 'transform 0.2s'

                    })
                    break
                case (refDeplacement >= 1) && (refDeplacement < 10): //s'il va vers la droite
                $('.imageToMove').remove()
                $('.circleCssContainer').remove()
                    weaponPlayerDetection(1, 2, 3, -10, +1, +10)
                    $('#img' + playerId).css({
                        transform: 'rotate(90deg)',
                        transition: 'transform 0.2s'
                    })
                    break
                case (refDeplacement <= -1) && (refDeplacement > -10): //s'il va à gauche
                $('.imageToMove').remove()
                $('.circleCssContainer').remove()
                    weaponPlayerDetection(-1, -2, -3, -10, -1, +10)
                    $('#img' + playerId).css({
                        transform: 'rotate(270deg)',
                        transition: 'transform 0.2s'

                    })
                    break

                    function weaponPlayerDetection(i, j, k, l, m, n) {
                        switch (refDeplacement) { //vérifie de combien de cases on se déplace
                            case k: //si on se déplace de 3 cases
                                movingFirstCase()
                                movingSecondCase()
                                movingThirdCase()
                                break
                            case j: //2 cases
                                movingFirstCase()
                                movingSecondCase()
                                break
                            case i: //1 case
                                movingFirstCase()
                                break
                        }

                        function movingFirstCase() {
                            posTop = $('td')[ancienNumeroCaseJoueur + i].getAttribute('top');
                            posLeft = $('td')[ancienNumeroCaseJoueur + i].getAttribute('left');
                            movePlayer(posTop, posLeft) //déplacement d'une case 
                            if ($('td')[ancienNumeroCaseJoueur + i + l]) {
                                if ($('td')[ancienNumeroCaseJoueur + i].getAttribute('weapon') === 'true') {
                                    setTimeout(() => {
                                        $('#' + $('td')[ancienNumeroCaseJoueur + i].getAttribute('weapontype')).css('display', 'none') // '#' permet de récupérer un id + le nom de l'arme qui correspond à la case sur laquelle passe le joueur
                                        changeWeapon($('#' + $('td')[ancienNumeroCaseJoueur + i].getAttribute('weapontype'))[0].id, numberPlayer)
                                    }, 100);
                                }
                                if ($('td')[ancienNumeroCaseJoueur + i + l].getAttribute('playeronit') === playeronitCheck) {
                                    setTimeout(function () {
                                        switch (checkCases0or9(ancienNumeroCaseJoueur, i)) {
                                            case true:
                                                fight()
                                                break
                                        }
                                    }, 100)
                                } else if ($('td')[ancienNumeroCaseJoueur + i + m]) {
                                    if ($('td')[ancienNumeroCaseJoueur + i + m].getAttribute('playeronit') === playeronitCheck) {
                                        setTimeout(function () {
                                            switch (checkCases0or9(ancienNumeroCaseJoueur, i)) {
                                                case true:
                                                    fight()
                                                    break
                                            }
                                        }, 100)
                                    } else if ($('td')[ancienNumeroCaseJoueur + i + n]) {
                                        if ($('td')[ancienNumeroCaseJoueur + i + n].getAttribute('playeronit') === playeronitCheck) {
                                            setTimeout(function () {
                                                switch (checkCases0or9(ancienNumeroCaseJoueur, i)) {
                                                    case true:
                                                        fight()
                                                        break
                                                }
                                            }, 100)
                                        }
                                    }
                                }
                            }
                        }

                        function movingSecondCase() {
                            posTop = $('td')[ancienNumeroCaseJoueur + j].getAttribute('top');
                            posLeft = $('td')[ancienNumeroCaseJoueur + j].getAttribute('left');
                            movePlayer(posTop, posLeft)

                            if ($('td')[ancienNumeroCaseJoueur + j].getAttribute('playeronit') === playeronitCheck) {
                                setTimeout(function () {
                                    switch (checkCases0or9(ancienNumeroCaseJoueur, j)) {
                                        case true:
                                            fight()
                                            break
                                    }
                                }, 700)
                            }

                            if ($('td')[ancienNumeroCaseJoueur + j + l]) {
                                if ($('td')[ancienNumeroCaseJoueur + j].getAttribute('weapon') === 'true') {
                                    setTimeout(() => {
                                        $('#' + $('td')[ancienNumeroCaseJoueur + j].getAttribute('weapontype')).css('display', 'none')
                                        changeWeapon($('#' + $('td')[ancienNumeroCaseJoueur + j].getAttribute('weapontype'))[0].id, numberPlayer)
                                    }, 700);
                                }
                                if ($('td')[ancienNumeroCaseJoueur + j + l].getAttribute('playeronit') === playeronitCheck) {
                                    setTimeout(function () {
                                        switch (checkCases0or9(ancienNumeroCaseJoueur, j)) {
                                            case true:
                                                fight()
                                                break
                                        }
                                    }, 700)
                                } else if ($('td')[ancienNumeroCaseJoueur + j + m]) {
                                    if ($('td')[ancienNumeroCaseJoueur + j + m].getAttribute('playeronit') === playeronitCheck) {
                                        setTimeout(function () {
                                            switch (checkCases0or9(ancienNumeroCaseJoueur, j)) {
                                                case true:
                                                    fight()
                                                    break
                                            }
                                        }, 700)
                                    } else if ($('td')[ancienNumeroCaseJoueur + j + n]) {
                                        if ($('td')[ancienNumeroCaseJoueur + j + n].getAttribute('playeronit') === playeronitCheck) {
                                            setTimeout(function () {
                                                switch (checkCases0or9(ancienNumeroCaseJoueur, j)) {
                                                    case true:
                                                        fight()
                                                        break
                                                }
                                            }, 700)
                                        }

                                    }

                                }
                            }
                        }

                        function movingThirdCase() {
                            posTop = $('td')[ancienNumeroCaseJoueur + k].getAttribute('top');
                            posLeft = $('td')[ancienNumeroCaseJoueur + k].getAttribute('left');
                            movePlayer(posTop, posLeft)
                            if ($('td')[ancienNumeroCaseJoueur + k].getAttribute('playeronit') === playeronitCheck) {
                                setTimeout(function () {
                                    switch (checkCases0or9(ancienNumeroCaseJoueur, k)) {
                                        case true:
                                            fight()
                                            break
                                    }
                                }, 1200)
                            }

                            if ($('td')[ancienNumeroCaseJoueur + k + l]) {
                                if ($('td')[ancienNumeroCaseJoueur + k].getAttribute('weapon') === 'true') {
                                    setTimeout(() => {
                                        $('#' + $('td')[ancienNumeroCaseJoueur + k].getAttribute('weapontype')).css('display', 'none')
                                        changeWeapon($('#' + $('td')[ancienNumeroCaseJoueur + k].getAttribute('weapontype'))[0].id, numberPlayer)
                                    }, 1200);
                                }
                                if ($('td')[ancienNumeroCaseJoueur + k + l].getAttribute('playeronit') === playeronitCheck) {
                                    console.log(playeronitCheck)
                                    setTimeout(function () {
                                        switch (checkCases0or9(ancienNumeroCaseJoueur, k)) {
                                            case true:
                                                fight()
                                                break
                                        }
                                    }, 1200)
                                } else if ($('td')[ancienNumeroCaseJoueur + k + m]) {
                                    if ($('td')[ancienNumeroCaseJoueur + k + m].getAttribute('playeronit') === playeronitCheck) {
                                        setTimeout(function () {
                                            switch (checkCases0or9(ancienNumeroCaseJoueur, k)) {
                                                case true:
                                                    fight()
                                                    break
                                            }
                                        }, 1200)
                                    } else if ($('td')[ancienNumeroCaseJoueur + k + n]) {
                                        if ($('td')[ancienNumeroCaseJoueur + k + n].getAttribute('playeronit') === playeronitCheck) {
                                            setTimeout(function () {
                                                switch (checkCases0or9(ancienNumeroCaseJoueur, k)) {
                                                    case true:
                                                        fight()
                                                        break
                                                }
                                            }, 1200)
                                        }

                                    }

                                }
                            }
                        }
                    }
            }

            //on assigne ces coordonnées à la position de playerElt
            function movePlayer(posTop, posLeft) {
                $('.player')[numberPlayer].setAttribute('style', `top: ${posTop}px; left: ${posLeft}px;`);
                
            }

            if (numberPlayer === 0) {
                setTimeout(() => {
                    play(1)
                }, 1200);
            } else {
                setTimeout(() => {
                    play(0)
                }, 1200);
            }
        });
    }





    function checkCases0or9(a, b) {

        let player1NewNum = Number($('#player1')[0].getAttribute('numbercase'));
        let player2NewNum = Number($('#player2')[0].getAttribute('numbercase'));

        let playerNumTransition = a + b;

        let arrCase0 = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
        let arrCase9 = [9, 19, 29, 39, 49, 59, 69, 79, 89, 99];

        let arrResults = [];

        for (i = 0; i < arrCase0.length; i++) {
            if ((((arrCase9[i] === player1NewNum) && (arrCase0[i] === playerNumTransition)) || ((arrCase9[i] === playerNumTransition) && (arrCase0[i] === player1NewNum))) || (((arrCase9[i] === player2NewNum) && (arrCase0[i] === playerNumTransition)) || ((arrCase9[i] === playerNumTransition) && (arrCase0[i] === player2NewNum))))
            {
                arrResults.push(false);
            } else {
                arrResults.push(true);
            }
        }
        for (i = 0; i < arrResults.length; i++) {
            if (arrResults[i] === false) {
                return false
            }
        }
        return true
    }


}