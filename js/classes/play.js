function play(numberPlayer) {
    let numeroCaseJoueur = Number($('.player')[numberPlayer].getAttribute('numberCase')); //le numéro de la case où se trouve le joueur
    
    detectionCases(numeroCaseJoueur);
    movements(numberPlayer);
    colorCaseToMove()
}