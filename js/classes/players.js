class Player {
    constructor(nom, sante, force) {
        this.nom = nom;
        this.sante = sante;
        this.force = force;
        this.arme = 'poings';
        this.defense = false;
    }
    // Attaque une cible
    attaquer(cible) {
        if (this.sante > 0) {
            let degats = this.force;
            if (cible.defense === true) {
                degats = (degats/2)
                cible.sante -= degats;
                cible.defense = false
            } else {
                cible.sante -= degats;
            }
            if (cible.sante > 0) {
                console.log(`${cible.nom} a encore ${cible.sante} points de vie`);
            } else {
                cible.sante = 0;
                console.log(`${this.nom} a tué ${cible.nom}`)
                    $('.buttonGame').css('display', 'none')
                    endBattle()
            }
        } else {
            console.log(
                `${this.nom} n'a plus de points de vie et ne pas pas attaquer`
            );
        }

    }
    decrire () {
        return `${this.nom} possède ${this.sante} points de santé et ${this.force} points de force`
    }
    armePortee () {
        return `Arme portée : ${this.arme}`
    }
}

let player1 = new Player('Zelda', 100, 5);
let player2 = new Player('Miko', 100, 5);
