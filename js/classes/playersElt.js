function createPlayerElt1() {

    let bodyElt = $('#bodyElt');

    let arrTdElt = $('td').get(); //crée un tableau des cases non-bloquées

    function randomCoordinates() { //renvoie des coordonnées au hasard de cases disponible et met à jour les cases dispo
        let arrCaseDispo = []; //tableau qui stockera les cases disponible=true
        for (let i = 0; i < arrTdElt.length; i++) { //on itère sur le tableau des cases non bloquées
            let statutCase = arrTdElt[i].getAttribute('disponible'); //on récupère le statut des cases non bloquées
            if (statutCase === 'true') { //si les cases sont disponibles...
                arrCaseDispo.push(arrTdElt[i]); //on met dans le tableau des cases dispo la case concernées
            }
        }
        let randomInt = Math.floor(Math.random() * arrCaseDispo.length) //un int au hasard basé sur la taille du tableau des cases dispo
        let numberCaseDispo = Number(arrCaseDispo[randomInt].getAttribute('number'));

        arrTdElt[numberCaseDispo].setAttribute('disponible', 'false'); //l'attribut "disponible" passe à "false" dans pour la case qui se trouve dans le tableau des cases non-bloquées

        let arrNum = [1, 2, 3, -1, -2, -3, +8, +9, +10, +11, +12, -8, -9, -10, -11, -12]; //tableau avec les numéro des cases à bloquer autour du joueur
        for (i = 0; i < arrNum.length; i++) {
            if ((arrTdElt[numberCaseDispo + arrNum[i]]) != undefined) { //si la case existe...
                arrTdElt[numberCaseDispo + arrNum[i]].setAttribute('disponible', 'false'); //elle devient indisponible
            }
        }
        return [arrCaseDispo[randomInt].getAttribute('top'), arrCaseDispo[randomInt].getAttribute('left'), numberCaseDispo]; //on renvoie les coordonnées top et left de la case disponible, basée sur le randomInt
    }

    function createPlayerElt(imgName, playerId, randomCoordinates) { //3e et dernier argument : tableau return au hasard
        let playerElt = jQuery('<div></div');
        let playerImg = jQuery('<img>');
        playerImg.attr('src', 'images/' + `${imgName}`);
        playerImg.attr('class', 'playerImgClass');
        playerImg.attr('id', `img${playerId}`);
        playerElt.append(playerImg);
        playerElt.attr({
            id: playerId,
            numberCase: randomCoordinates[2],
            class: 'player'
        });
        bodyElt.css({
            //  'position': 'relative'
        });
        bodyElt.append(playerElt);
        let posTop = randomCoordinates[0];
        let posLeft = randomCoordinates[1];

        let numberPlayer = ''
        if (playerId === 'player1') {
            numberPlayer = 0
        } else {
            numberPlayer = 1
        }

        $('td')[randomCoordinates[2]].setAttribute(`playeronit`, numberPlayer + 'a');
        

        return playerElt.css({
            'top': `${posTop}px`,
            'left': `${posLeft}px`
        })
    }


    //placer le joueur 2 dans une case dispo au hasard
    let player2Elt = createPlayerElt('player1.png', 'player1', randomCoordinates());
    let player1Elt = createPlayerElt('player2.png', 'player2', randomCoordinates());

}


