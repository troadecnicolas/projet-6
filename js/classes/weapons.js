function changeWeapon(weapon, playerNumber) {
    let player = '';
    switch (playerNumber) {
        case 0:
            switch (weapon) {
                case 'swordElt':
                    player1.force = 10
                    player1.arme = 'épée'
                    $('#armeDescription0').text(player1.armePortee())
                    $('#playerCard0').text(player1.decrire())
                    break
                case 'bardicheElt':
                    player1.force = 12
                    player1.arme = 'bardiche'
                    $('#armeDescription0').text(player1.armePortee())
                    $('#playerCard0').text(player1.decrire())

                    break
                case 'hammerElt':
                    player1.force = 14
                    player1.arme = 'marteau'
                    $('#armeDescription0').text(player1.armePortee())
                    $('#playerCard0').text(player1.decrire())

                    break
                case 'axeElt':
                    player1.force = 13
                    player1.arme = 'hache'
                    $('#armeDescription0').text(player1.armePortee())
                    $('#playerCard0').text(player1.decrire())

                    break
            }
            break
        case 1:
            switch (weapon) {
                case 'swordElt':
                    player2.force = 10
                    player2.arme = 'épée'
                    $('#armeDescription1').text(player2.armePortee())
                    $('#playerCard1').text(player2.decrire())

                    break
                case 'bardicheElt':
                    player2.force = 12
                    player2.arme = 'bardiche'
                    $('#armeDescription1').text(player2.armePortee())
                    $('#playerCard1').text(player2.decrire())

                    break
                case 'hammerElt':
                    player2.force = 14
                    player2.arme = 'marteau'
                    $('#armeDescription1').text(player2.armePortee())
                    $('#playerCard1').text(player2.decrire())

                    break
                case 'axeElt':
                    player2.force = 13
                    player2.arme = 'hache'
                    $('#armeDescription1').text(player2.armePortee())
                    $('#playerCard1').text(player2.decrire())

                    break
            }
            break
    }

}