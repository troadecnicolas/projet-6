function createWeapons() {

    let bodyElt = $('#bodyElt');
    let arrTdElt = $('td').get(); //crée un tableau des cases non-bloquées

    function randomCoordinates() { //renvoie des coordonnées au hasard de cases disponible et met à jour les cases dispo
        let arrCaseDispo = []; //tableau qui stockera les cases disponible=true
        for (let i = 0; i < arrTdElt.length; i++) { //on itère sur le tableau des cases non bloquées
            let statutCase = arrTdElt[i].getAttribute('disponible'); //on récupère le statut des cases non bloquées
            if (statutCase === 'true') { //si les cases sont disponibles...
                arrCaseDispo.push(arrTdElt[i]); //on met dans le tableau des cases dispo la case concernées
            }
        }
        let randomInt = Math.floor(Math.random() * arrCaseDispo.length) //un int au hasard basé sur la taille du tableau des cases dispo
        arrTdElt[arrCaseDispo[randomInt].getAttribute('number')].setAttribute('disponible', 'false'); //l'attribut "disponible" passe à "false" dans pour la case qui se trouve dans le tableau des cases non-bloquées
        return [arrCaseDispo[randomInt].getAttribute('top'), arrCaseDispo[randomInt].getAttribute('left'), arrCaseDispo[randomInt]]; //on renvoie les coordonnées top et left de la case disponible, basée sur le randomInt
    }

    function createWeaponElt(imgName, weaponId, randomCoordinates) { //3e et dernier argument : tableau return au hasard
        randomCoordinates[2].setAttribute('weapon', true)
        randomCoordinates[2].setAttribute('weaponType', weaponId)
        let weaponElt = jQuery('<div></div');
        let weaponImg = jQuery('<img>');
        weaponImg.attr('src', 'images/' + `${imgName}`);
        weaponImg.attr('class', 'weaponImg');
        weaponImg.css({
            width: '60px',
            height: '60px'
            //    'position': 'absolute'
        });
        weaponElt.append(weaponImg);
        weaponElt.attr({
            id: `${weaponId}`,
            class: 'weapon'
        });
        weaponElt.css({
            width: '60px',
            height: '60px'
        })

        bodyElt.css({
            //    'position': 'relative'
        });
        bodyElt.append(weaponElt);

        return weaponElt.css({
            top: `${Number(randomCoordinates[0])}px`,
            left: `${Number(randomCoordinates[1])}px`
            //    'position': 'absolute'
        })
    }


    let axeElt = createWeaponElt('axe.png', 'axeElt', randomCoordinates()); //mettre le tableau return au hasard dans l'appel de fonction
    let swordElt = createWeaponElt('sword.png', 'swordElt', randomCoordinates());
    let bardicheElt = createWeaponElt('bardiche.png', 'bardicheElt', randomCoordinates());
    let hammerElt = createWeaponElt('hammer.png', 'hammerElt', randomCoordinates());

}
