$(document).ready(function () {


    let plateau = $('#plateau');
    let body = $('#bodyElt');
    let surcouche = $('#surcouche')

    genererPlateau();
    randomisePlateau()

   

    function genererSurcouche() {
        for (i = 0; i <= 9; i++) {
            let tr = jQuery('<tr></tr>');
            tr.attr('id', i).attr('class', 'surcouchetrClass');
            surcouche.append(tr);
            for (j = 0; j <= 9; j++) {
                let td = jQuery('<td></td>');
                td.attr({
                    id: j,
                    class: 'tdSurcouche'
                    //    position: 'absolute'
                });
                td.css({
                    backgroundSize: 'contain'
                });
                tr.append(td);
            }
        };
    }
    genererSurcouche()

    function borderTd() {
        $('.nonBlocked').css({
            // 'border': 'solid 1px black',
            'width': '70px',
            'height': '70px'
        });
    }
    borderTd();
    $('td').map(function (index) {
        $(this).attr({
            number: index,
            top: `${$(this).position().top}`,
            left: `${$(this).position().left}`
        })
    })

    $('td').each(function (index) {
        if (this.getAttribute('class') === 'tdSurcouche') {
            this.removeAttribute('top');
            this.removeAttribute('left');
        }
    })

    plateau.append(surcouche);

    createPlayerElt1();
    createWeapons();
    let card1 = createCards(0)
    let card2 = createCards(1)


    play(0)
});